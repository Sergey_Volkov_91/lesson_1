

Задание 2.
Знакомство с Figma. Скачать, установить. Ознакомиться с интерфейсом для последующей работы с макетом.
figma.com

Задание 3.

Создать ветку на Git Hub в моем репозитории c названием Z12-onl-Ivanov-Ivan


Задание 4. 
Скачать и настроите VS Code.
Ссылка на видео по настройке VS Code
https://youtu.be/QeUp3CahkQw

!!!!!  Задание 5.
Знакомство с методологией БЭМ. Постепенное введение в практику.
https://ru.bem.info/methodology/quick-start/

Задание 6. 
Список наиболее встречающихся классов в HTML/CSS

!!!!!  Знакомство с тегами. Синтаксис, назначение.
Ссылка на справочник тегов
https://webref.ru/html

Список тегов:
      <aside></aside>
      <article></article>
      <audio src=""></audio>
      <body></body>
      <button></button>
      <footer></footer>
      <form action=""></form>
      <h1></h1>
      <head></head>
      <header></header>
      <html></html>
      <img src="" alt="">
      <input type="text">
      <label for=""></label>
      <li></li>
      <link rel="stylesheet" href="">
      <main></main>
      <menu></menu>
      <meta>
      <nav></nav>
      <p></p>
      <script></script>
      <select name="" id=""></select>
      <section></section>
      <span></span>
      <textarea name="" id="" cols="30" rows="10"></textarea>
      <title></title>
      <ul></ul>
      <video src=""></video>